// eslint-disable-next-line no-use-before-define
var sapientApp = sapientApp || {};
var JSON_FILE;
var JSON_DATA;

sapientApp.productObj = {
  data: null,
  flagContainer: true,
  cartList: [],
  init: function (JSON_FILE) {
    sapientApp.productObj.loadJSON();
    sapientApp.productObj.bindEvents();
    sapientApp.searchObj.init();
  },
  loadJSON: function () {
    if (typeof JSON_FILE !== 'undefined' && JSON_FILE !== '') {
      $.ajax({
        url: JSON_FILE,
        type: 'get',
        dataType: 'json',
        success: function (data) {
          var flag = sapientApp.productObj.flagContainer;
          sapientApp.productObj.data = data;
          JSON_DATA = data;
          sapientApp.productObj.createView().getLoadView(data, flag);
        },
        complete: function () {

        }
      });
    }
  },
  createView: function () {
    var NSObj = sapientApp.productObj;
    function initLoadView (objData, flag) {
      var productViewDefault = '';
      var productViewLatest = '';
      productViewDefault += '<section class="clearfix"><h2>Products</h2>';
      if (flag) {
        productViewLatest += '<section class="clearfix"><h2>Latest Products</h2>';
      }
      $(objData.products).each(function (i, item) {
        var arrivals = (objData.products[i].arrivals === 'default') ? 'default' : 'latest';
        var image = item.image.small;
        var imageBig = item.image.big;
        var zoomImage = item.image.zoom;
        var title = item.title;
        var price = item.price;

        var articleContainer = '<article>' +
'<figure>' +
'<img src="' + image + '" class="icon productImg" data-img-url="' + imageBig + '" data-zoom-image="' + zoomImage + '" alt="" data-img-desc="' + item.desc + '"  data-img-price="' + price + '"  />' +
'</figure>' +
'<h3>' + title + '</h3>' +
'<span>$' + price + '</span>' +
'<a href="#" class="btn blue addCart" data-product-id=' + (i + 1) + '>Add to cart</a>' +
'</article>';
        if (arrivals === 'latest' && flag) { productViewLatest += articleContainer; }
        productViewDefault += articleContainer;
      });
      productViewDefault += '</section>';
      productViewLatest += '</section>';
      $('.content-panel').empty().append(productViewLatest + productViewDefault);
    }

    function viewCartList () {
      var totalCart = NSObj.cartList.length;
      var totalPrice = 0;
      var GridView = '<div class="list-container"><table>' +
'<tr>' +
'<th>&nbsp;</th>' +
'<th>ITEM</th>' +
'<th>QTY</th>' +
'<th>PRICE</th>' +
'<th>DELIVERY DETAILS</th>' +
'<th>SUBTOTAL</th>' +
'<th>&nbsp;</th>' +
'</tr>';
      for (var i = 0; i < totalCart;) {
        totalPrice += NSObj.cartList[i].price;
        var zoomImage = NSObj.cartList[i].image.zoom;
        GridView += '<tr>' +
'<td><img src="' + NSObj.cartList[i].image.small + '" data-zoom-image="' + zoomImage + '" class="icon" alt="" /></td>' +
'<td>' + NSObj.cartList[i].category.capitalize() + '</td>' +
'<td>1</td>' +
'<td>' + NSObj.cartList[i].price + '</td>' +
'<td></td>' +
'<td>' + NSObj.cartList[i].price + '</td>' +
'<td data-product-id=' + (i + 1) + '><img class="removeDataFromCart" src="img/icons/remove.png"></td>' +
'</tr>';
        i++;
      }
      GridView += '</table></div>';
      GridView += '<div><h3 style="text-align:right">Estimated Total : Rs.' + totalPrice + '</h3></div>';
      $('.popup-container').append(GridView);
      $('.popup-container img[data-zoom-image!=""]:not(.removeDataFromCart)').elevateZoom({ easing: true });
    }
    function createDetailView (dataId, imgSrc, imgDesc, imgPrice, imgZoom) {
      var productDetails = '';
      var imageThumbs = '';
      var colors = '';
      var zoomImage = imgZoom;
      var bigImage = imgSrc.replace('small', 'big');
      $(JSON_DATA.products).each(function (i, item) {
        if (parseInt(item.id) === dataId) {
          $(item.image.detailimg).each(function (index, itemimg) {
            imageThumbs += '<li><img src="' + itemimg + '" class="icon" /></li>';
          });
          $(item.options.color).each(function (index, itemcolor) {
            colors += '<span style="background :' + itemcolor + '"></span>';
          });
        }
      });
      productDetails += '<div class="cart-details-view">' +
'<article>' +
'<figure>' +
'<img src="' + bigImage + '"  data-zoom-image="' + zoomImage + '"  class="icon" />' +
'</figure>' +
'<div class="thumbnails clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">' +
'<input type="button" value="Prev" class="prev inactive">' +
'<div>' +
'<ul>' + imageThumbs + '</ul>' +
'</div>' +
'<input type="button" value="Next" class="next">' +
'</div>' +
'</article>' +
'<article class="about-product-details">' +
'<h1> Details about products : </h1>' +
'<h2>' + imgDesc + '</h2>' +
'<h3> Rs.  ' + imgPrice + '</h3>' +
'<p><strong>SELECT COLOR : </strong><br />' + colors + '</p>' +
'<div class="checkout"><a href="#" class="btn blue">Checkout</a></div>' +
      '</article>' +
'</div>';
      $('.popup-container').append(productDetails).show();
      $('.popup-container article figure img[data-zoom-image!=""]').elevateZoom({ easing: true });
    }
    return {
      getLoadView: function (objData, flag) {
        initLoadView(objData, flag);
      },
      getCartListView: function () {
        viewCartList();
      },
      getCartDetailView: function (index, imgURL, imgDetails, imgPrice, imgZoom) {
        createDetailView(index, imgURL, imgDetails, imgPrice, imgZoom);
      },
      deleteRemoveList: function (index) {
        NSObj.cartList.remove(NSObj.cartList[index]);
        if (('localStorage' in window)) {
          NSObj.localStorageMethod().setStorage('sapientAppCartStorage', NSObj.cartList);
        }
      }
    };
  },
  bindEvents: function () {
    var NSObj = sapientApp.productObj;
    var actualImageUrl;
    sapientApp.productObj.localStorageMethod().getCartData('sapientAppCartStorage');
    $('.content-panel').on('click', '.addCart', function (event) {
      event.preventDefault();
      var index = $(this).data('product-id');
      NSObj.cartList.push(JSON_DATA.products[index - 1]);
      /*   ************* removing duplicates from Arrays *************   */
      NSObj.cartList = NSObj.cartList.filter(function (item, index, inputArray) {
        return inputArray.indexOf(item) === index;
      });
      var totalItemInCart = NSObj.cartList.length;
      if (('localStorage' in window)) {
        NSObj.localStorageMethod().setStorage('sapientAppCartStorage', NSObj.cartList);
      }
      $('.cartList').find('span').empty().html(totalItemInCart);
    });

    $('.content-panel').on('click', '.productImg', function (event) {
      var index = $(this).closest('article').find('.addCart').data('product-id');
      var imgURL = $(this).data('img-url');
      var imgDetails = $(this).data('img-desc');
      var imgPrice = $(this).data('img-price');
      var zoomImage = $(this).data('zoom-image');
      NSObj.createView().getCartDetailView(index, imgURL, imgDetails, imgPrice, zoomImage);
    });

    $('.container').on('click', '.cartList', function (event) {
      event.preventDefault();
      if (NSObj.cartList.length <= 0) { return false; }
      var body = $('body');
      body.toggleClass('menu-opened').hasClass('menu-opened') ? $('.overlay').fadeIn() : $('.overlay').fadeOut();/**/
      $('.popup-container').show();
      NSObj.createView().getCartListView();
    });
    $('.popup-container').on('click', '.b-close', function (event) {
      $(this).closest('.popup-container').hide().find('div').remove();
      $('.overlay').hide();
      $('body').toggleClass('menu-opened');
    });
    $('.popup-container').on('click', '.removeDataFromCart', function (event) {
      var index = $(this).closest('td').data('product-id');
      NSObj.createView().deleteRemoveList(index - 1);
      $('.popup-container').find('div').remove();
      NSObj.createView().getCartListView();
      var totalItemInCart = NSObj.cartList.length;
      $('.cartList').find('span').empty().html(totalItemInCart);
      if (NSObj.cartList.length <= 0) {
        $('.popup-container .b-close').trigger('click');
      }
    });
    $('.popup-container').on('click', '.prev:not(.inactive):not(.progress)', function (event) {
      event.preventDefault();
      var that = $(this);
      $(this).addClass('progress');
      $('.thumbnails ul').animate({
        left: '+=210'
      }, 3000, function () {
        that.removeClass('progress');
      });
    });

    $('.popup-container').on('click', '.next:not(.progress)', function (event) {
      event.preventDefault();
      var that = $(this);
      $(this).addClass('progress');

      $('.thumbnails ul').animate({
        left: '-=210'
      }, 3000, function () {
        $('.popup-container').find('.prev').removeClass('inactive');
        that.removeClass('progress');
      });
    });

    $('.popup-container').on('mouseenter', '.thumbnails img', function () {
      actualImageUrl = $('.cart-details-view figure').find('img').attr('src');
      $('.cart-details-view figure').find('img').attr({ src: $(this).attr('src') });
    }).on('mouseleave', '.thumbnails img', function () {
      $('.cart-details-view figure').find('img').attr({ src: actualImageUrl });
    });
  },
  localStorageMethod: function () {
    function receiveStorageData (keyName) {
      if (localStorage && localStorage.getItem(keyName)) {
        sapientApp.productObj.cartList = sapientApp.productObj.localStorageMethod().getStorageObj(keyName);
        var totalItemInCart = sapientApp.productObj.cartList.length;
        $('.cartList').find('span').empty().html(totalItemInCart);
      }
    }
    return {
      deleteStorage: function (keyName) {
        localStorage.removeItem(keyName);
      },
      setStorage: function (keyName, obj) {
        localStorage.setItem(keyName, JSON.stringify(obj));
      },
      getStorageObj: function (keyName) {
        return JSON.parse(localStorage[keyName]);
      },
      getCartData: function (keyName) {
        receiveStorageData(keyName);
      }
    };
  }
};

Array.prototype.remove = function () {
  var what; var a = arguments; var L = a.length; var ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};
/*  Remove the duplicate value from Arrays   */
// eslint-disable-next-line no-unused-vars
function removeDuplicate (array) {
  var index = {};
  for (var i = array.length - 1; i >= 0; i--) {
    if (array[i] in index) {
      array.splice(i, 1);
    } else {
      index[array[i]] = true;
    }
  }
}

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

$(document).ready(function () {
  JSON_FILE = 'json/product.json';
  sapientApp.productObj.init(JSON_FILE);
});
