/**
 * [sapientApp Search and Filter]
 */
var sapientApp = sapientApp || {};

sapientApp.search = {
  init: function (config) {
    document.getElementById(config.search).onkeypress = this.search;
    $('select').change(this.factory);
  },

  search: function (event) {
    console.log(event.target.value);
  },

  factory: function (event) {
    event.target.value();
  },
  productName: function () {},
  productBrand: function () {},
  productPrice: function () {},
  productLatest: function () {}
};

sapientApp.search.init({
  performsearch: ['name', 'price', 'id']
});
