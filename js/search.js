// eslint-disable-next-line no-use-before-define
var sapientApp = sapientApp || {};
var filterDataObj;
var JSON_DATA = sapientApp.productObj.data;
sapientApp.searchObj = {
  init: function () {
    sapientApp.searchObj.bindEvents();
  },
  searchItem: function (searchValue) {
    if (searchValue.length < 3) { sapientApp.productObj.createView().getLoadView(JSON_DATA, true); return false; }
    // var titleArray = [];
    var searchObj = {};
    searchObj.products = [];
    $(JSON_DATA.products).each(function (i, item) {
      if (new RegExp(searchValue.toLowerCase()).test(JSON_DATA.products[i].title.toLowerCase())) {
        searchObj.products.push(JSON_DATA.products[i]);
      }
    });
    if (searchObj.products.length <= 0) {
      sapientApp.productObj.createView().getLoadView(JSON_DATA, true);
    } else {
      sapientApp.productObj.createView().getLoadView(searchObj, false);
    }

    return searchObj;
  },
  filterItem: function () {
    var fitlerObj = {};
    function productName (selectedValue) {
      fitlerObj.products = [];
      $(JSON_DATA.products).each(function (i, item) {
        if (selectedValue.toLowerCase() === JSON_DATA.products[i].category.toLowerCase()) {
          fitlerObj.products.push(JSON_DATA.products[i]);
        }
      });
      fitlerObj = (fitlerObj.products.length > 0) ? fitlerObj : JSON_DATA;
      return fitlerObj;
    }
    function productPrice (selectedValue) {
      var minPrice = selectedValue.split('-')[0];
      var maxPrice = selectedValue.split('-')[1];
      fitlerObj.products = [];
      $(JSON_DATA.products).each(function (i, item) {
        if (minPrice < parseInt(JSON_DATA.products[i].price) && (maxPrice > parseInt(JSON_DATA.products[i].price) || maxPrice === undefined)) {
          fitlerObj.products.push(JSON_DATA.products[i]);
        }
      });
      fitlerObj = (fitlerObj.products.length > 0) ? fitlerObj : JSON_DATA;
      return fitlerObj;
    }
    function productLatest (selectedValue) {
      if (selectedValue === 'descending') {
        filterDataObj.products.sort();
        filterDataObj.products.reverse();
      } else {
        filterDataObj.products.reverse();
      }
      return filterDataObj;
    }
    return {
      getProductName: function (selectedValue) {
        return productName(selectedValue);
      },
      getProductPrice: function (selectedValue) {
        return productPrice(selectedValue);
      },
      getProductLatest: function (selectedValue) {
        return productLatest(selectedValue);
      }
    };
  },
  bindEvents: function () {
    $('input[type=search]').on('keyup', function () {
      sapientApp.searchObj.searchItem($(this).val());
    });

    $('select').on('change', function () {
      var filterType = $(this).prop('name');
      var selectedValue = $(this).val();

      switch (filterType) {
      case ('productName'):
        filterDataObj = sapientApp.searchObj.filterItem().getProductName(selectedValue);
        break;
      case ('productPrice'):
        filterDataObj = sapientApp.searchObj.filterItem().getProductPrice(selectedValue);
        break;
      case ('latestProduct'):
        filterDataObj = sapientApp.searchObj.filterItem().getProductLatest(selectedValue);
        break;
      }
      sapientApp.productObj.createView().getLoadView(filterDataObj, false);
    });
  }
};
