/*  The "wrapper" function  */
module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: ';',
			},
			dist: {
				src: [
					'scripts/plugins/**/*.js',
					'scripts/utils/**/*.js',
					'scripts/app.js',
					'js/main.js',
					'js/search.js'
				],
				dest: 'dist/scripts/main.js',
			},
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: 'dist/scripts/main.js',
				dest: 'build/scripts/main.min.js'
			}
		},
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Default task(s).
	grunt.registerTask('default', ['concat', 'uglify']);
};