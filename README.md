## Quick start

* Clone the git repo — `git clone https://ansinha@bitbucket.org/ansinha/productlisting.git`

* Install dependencies using command **npm install ** in terminal
* Setup the ESLint using command **./node_modules/.bin/eslint --init ** in terminal
* Parse the JavaScript Syntex by ESLint in command *./node_modules/.bin/eslint search.js ** in terminal
* To start build system use command **npm run start** 
* Make sure your local server are running , then open http://localhost:xyz/productlisting to view app in the browser.


